/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2024. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });

  // active navbar of page current
  var urlcurrent = window.location.pathname;
  $(".navbar-nav li a[href$='" + urlcurrent + "'],.faq-list ul li a[href$='" + urlcurrent + "']").addClass('active');
  $(window).on("load", function () {
    $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
  });
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $('header').addClass('scroll');
    } else {
      $('header').removeClass('scroll');
    }
  });
  $('.feedback-list').slick({
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    arrows: true
  });
  $('.faq-list .tab li a').on('click', function () {
    $('.faq-list .tab li a').removeClass('active');
  });

  //select
  $('.selected').on('click', function () {
    $('.box-select').removeClass('show');
    $(this).parent().toggleClass('show');
  });
  $('.select-list li').on('click', function () {
    $(this).parents('.box-select').find('.select-list li').removeClass('active');
    $(this).addClass('active');
    var img_icon = $(this).data('icon');
    var txt = $(this).data('name');
    var html_img = `<img src="${img_icon}">`;
    $(this).parents('.box-select').removeClass('show');
    $(this).parents('.box-select').find('.selected .icon').html('').append(html_img);
    $(this).parents('.box-select').find('.selected .name').html('').append(txt);
  });
  $(document).mouseup(function (e) {
    var container = $(".box-select");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $('.box-select').removeClass('show');
    }
  });
  $(".faq-list li").click(function () {
    $('html,body').animate({
      scrollTop: $(".box-answer").offset().top - 100
    }, 'slow');
  });
  AOS.init({
    easing: 'ease-out-back',
    duration: 1000,
    once: true,
    offset: 60
  });

  //accordion
  var accordion_content = $('.faq-list .item__content');
  var accordion__header = $('.faq-list .item__header');
  var accordion_active = $('.faq-list > .active');
  accordion_content.hide();
  accordion_active.find('.item__content').slideToggle();
  accordion__header.click(function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('active');
    if (accordion_content.parent('.active')) {
      $(this).parent().find('.item__content').slideToggle();
    }
    return false;
  });
});